<?php

namespace HappyHour\Util;

/**
 * Description of Sessao
 *
 * @author iftm
 */
class Sessao {

    function __construct() {
        
    }

    function start() {
        return session_start();
    }

    function add($chave, $valor) {
        $_SESSION['happy'][$chave] = $valor;
    }

    function get($chave) {
        if (isset($_SESSION['happy'][$chave]))
            return $_SESSION['happy'][$chave];
        return '';
    }

    function rem($chave) {
        if (isset($_SESSION['happy'][$chave]))
            session_unset($_SESSION['happy'][$chave]);
    }

    function del() {
        if (isset($_SESSION['happy']))
            session_unset($_SESSION['happy']);
        session_destroy();
    }

    function existe($chave) {
        if (isset($_SESSION['happy'][$chave]))
            return true;
        return false;
    }

}
