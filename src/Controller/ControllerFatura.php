<?php

namespace HappyHour\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;
use HappyHour\Entity\Cliente;
use HappyHour\Models\ModelCliente;
use OpenBoleto\Banco\BancoDoBrasil;
use OpenBoleto\Agente;
use HappyHour\Models\ModelComanda;
use Dompdf\Dompdf;

class ControllerFatura {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct($response, $twig, $request, $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function pagarAvista() {

        if ($this->sessao->existe('user')) {

            $arrayDeDadosDoForm = $this->request->get("dadosFormulario");
            $valorInfo = (float) $arrayDeDadosDoForm[0]['value'];
            $total = (float) $arrayDeDadosDoForm[1]['value'];
            $comandaId = (int) $arrayDeDadosDoForm[2]['value'];

            echo '<div class="alert alert-danger" role="alert">Valor insuficiente!</div>';


            if ($valorInfo >= $total) {
                $model = new ModelComanda();
                $model->pagarComanda($comandaId);
                print_r($valorInfo - $total);
            } else {
                echo '<div class="alert alert-danger" role="alert">Valor insuficiente!</div>';
            }
        } else {

            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function buscarCpf() {

        if ($this->sessao->existe('user')) {
            $arrayDeDadosDoForm = $this->request->get("dadosFormulario");
            $cpf = $arrayDeDadosDoForm[0]['value'];
            $model = new ModelCliente();
            $dado = $model->exibirCliente($cpf);
            $this->response->setContent($this->twig->render("buscarCpf.twig", ['dado' => $dados]));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function gerarBoleto() {

        if ($this->sessao->existe('user')) {
            $cpf = $this->request->get("cpf");
            $comanda = $this->request->get("comanda");
            $model = new ModelComanda();
            $valor = $this->request->get("valor");
            $cliente = new ModelCliente();
            $cliente = $cliente->exibirCliente($cpf);
            if ($cliente) {
                $sacado = new Agente($cliente['cli_nome'] . ' ' . $cliente['cli_sobrenome'], $cliente['cli_cpf'], $cliente['cli_rua'] . ' ' . $cliente['cli_numero'] . ' ' . $cliente['cli_bairro'], $cliente['cli_cep'], $cliente['cli_cidade'], $cliente['cli_estado']);
                $cedente = new Agente('Happy Hour', '02.123.123/0001-11', 'Rua Marechal Deodoro 154 - Centro ', '14600-000', 'São Joaquim da Barra', 'SP');
                $boleto = new BancoDoBrasil(array(
                    'valor' => $valor,
                    'sequencial' => 1234567, 
                    'sacado' => $sacado,
                    'cedente' => $cedente,
                    'agencia' => 1724, 
                    'carteira' => 18,
                    'conta' => 10403005, 
                    'convenio' => 1234, 
                ));
                $model->pagarComanda($comanda);
                $dompdf = new Dompdf();
                $dompdf->loadHtml($boleto->getOutput());
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $dompdf->stream();
            } else {
                echo '<div class="alert alert-danger" role="alert"><strong>Ops!</strong>Cliente não encontrado!</div>';
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }
}
