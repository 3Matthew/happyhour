<?php

namespace HappyHour\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;

class ControllerIndex {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function show() {
        if ($this->sessao->existe('user')) {
             $this->response->setContent($this->twig->render("indexlogado.twig"));
             return;
        }else{
        $this->response->setContent($this->twig->render("index.twig"));
        return;
        }
        
    }

    public function showHome() {
        
            $this->response->setContent($this->twig->render("indexlogado.twig"));
            return;
         
    }

    public function showLogin() {
        $this->response->setContent($this->twig->render("Login.twig"));
        return;
    }

    public function Logout() {

            
                $this->sessao->del();
                $this->response->setContent($this->twig->render("index.twig"));
                return;
            
    }

}
