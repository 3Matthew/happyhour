<?php

namespace HappyHour\Controller;

use HappyHour\Entity\Mesa;
use Twig\Environment;
use \HappyHour\Models\ModelMesa;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;

class ControllerMesa {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function exibirTelaMesa() {
        if ($this->sessao->existe('user')) {
            return $this->response->setContent($this->twig->render("CadastrarMesa.twig"));
        } else {
           $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function salvarMesa() {

        if ($this->sessao->existe('user')) {
            $Nmesa = $this->request->get("numesa");
            $QtdPessoa = $this->request->get("pessoas");
            $disponivel = $this->request->get("disponivel");

            if (isset($Nmesa) && !empty($Nmesa) && isset($QtdPessoa) && !empty($QtdPessoa)) {
                $mesa = new Mesa($Nmesa, $QtdPessoa, $disponivel);
                $model = new ModelMesa();
                $model->inserirMesa($mesa);
                $redirect = new RedirectResponse('/mesa/exibir/' . $Nmesa);
                $redirect->send();
            } else {
                $redirect = new RedirectResponse('/mesa/cadastrar');
                $redirect->send();
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function exibirMesa($id) {
        if ($this->sessao->existe('user')) {


            $model = new ModelMesa();
            $dados = $model->buscarMesa($id);
            return $this->response->setContent($this->twig->render("exibirMesa.twig", ['dados' => $dados]));
        } else {
           $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function mostrarTodas() {

        if ($this->sessao->existe('user')) {
            $model = new ModelMesa();
            $dados = $model->listarMesas();
            return $this->response->setContent($this->twig->render("mesas.twig", ['dados' => $dados]));
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

}
