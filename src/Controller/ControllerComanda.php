<?php

namespace HappyHour\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;
use HappyHour\Entity\Comanda;
use HappyHour\Models\ModelComanda;
use \HappyHour\Models\ModelProduto;
use DateTime;
use Dompdf\Dompdf;

class ControllerComanda {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function criarComanda($id) {
        if ($this->sessao->existe('user')) {
            $data = new DateTime();
            $comanda = new Comanda(null, $data, null, $id);
            $model = new ModelComanda();
            $model->criarComanda($comanda);
            $redirect = new RedirectResponse('/mesa/comandas/' . $id);
            $redirect->send();
        } else {
            $this->response->setContent($this->twig->render("index.twig"));
            return;
        }
    }

    public function exibirComandas($id) {

        if ($this->sessao->existe('user')) {
            $model = new ModelComanda();
            $comadas = $model->buscarComandasdaMesa($id);
            $this->response->setContent($this->twig->render("Comanda.twig", ['dados' => $comadas]));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function exibirProdutos($id) {

        if ($this->sessao->existe('user')) {
            $model = new ModelProduto();
            $dados = $model->listarProdutos();
            $this->response->setContent($this->twig->render("menu.twig", ['id' => $id, 'dados' => $dados]));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function addItensComanda() {

        if ($this->sessao->existe('user')) {

            $comanda = $this->request->get("comanda");
            $produto = $this->request->get("produto");
            $quantidade = $this->request->get("quantidade");


            if (isset($quantidade) && !empty($quantidade) && $quantidade > 0) {
                $model = new ModelComanda();
                $model->addItem($comanda, $produto, $quantidade);
                $redirect = new RedirectResponse('/comanda/' . $comanda);
                $redirect->send();
            } else {
                
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function verDetalhesComanda($id) {
        if ($this->sessao->existe('user')) {

            $model = new ModelComanda();
            $detalhes = $model->detalhesComanda($id);
            $total = $model->getTotalComanda($id);

            $this->response->setContent($this->twig->render("detalhesComanda.twig", ['id' => $id, 'dados' => $detalhes, 'total' => $total]));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function fecharComanda($id) {

        if ($this->sessao->existe('user')) {

            $model = new ModelComanda();
            $model->calcularComanda($id);
            $comanda = $model->buscarComanda($id);
            $this->response->setContent($this->twig->render("Fechamento.twig", ['dados' => $comanda]));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function gerarRelatorio() {


        if ($this->sessao->existe('user')) {


            $this->response->setContent($this->twig->render("relatorio.twig"));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }
    
    public function CalcularAvista() {
        
        if ($this->sessao->existe('user')) {

            $valorInfo = $this->request->get("valorInfo");
            $total = $this->request->get("total");
            $comanda = $this->request->get("comanda");
            if($valorInfo >= $total ){
                $valorInfo-= $total;
                $model = new ModelComanda();
                $dados = $model->buscarComanda($comanda);
                $model->pagarComanda($comanda);
                $this->response->setContent($this->twig->render("Fechamento.twig", ['dados' => $dados, 'troco' => $valorInfo ]));
            return;
            }
            
            
            
            
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
        
        
        
    }
    
    
    

    public function showRelatorio() {


        if ($this->sessao->existe('user')) {

            $datain = $this->request->get("datain");
            $datafim = $this->request->get("datafim");
            $datain = new DateTime($datain);
            $datafim = new DateTime($datafim);
            $datain = $datain->format('Y-m-d H:i:s');
            $datafim = $datafim->format('Y-m-d H:i:s');
            $model = new ModelComanda();

            $relatorio = $model->buscarRelatorio($datain, $datafim);

           $html = '<div class="table-responsive">
            <table style ="
    background-color: #f2f2f2;
    font-family: Arial, Helvetica, sans-serif;

        " class="table table-striped table-bordered">
                <thead style= "
                background-color:#050E33;color: white;
               
            "
             <h1 style="text-align:center;"> Relatório de Vendas </h1>            

                class="thead-dark">
                    <tr>
                        <th>Data </th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>';
           
           

        foreach ($relatorio as $key => $value) {
            $html .= ''
                    . ''
                    . '<tr style="background-color:#FFF;">'
                    . '<td>' . $value->cmd_data . '</td>'
                    . '<td>' . $value->cmd_total . '</td>';
        }

        $html .= "</tbody>";
            
            
            
            
            $dompdf = new Dompdf();
            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');
            $dompdf->render();
            $dompdf->stream();
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

}
