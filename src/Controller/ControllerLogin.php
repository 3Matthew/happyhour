<?php

namespace HappyHour\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;
use HappyHour\Entity\User;
use HappyHour\Models\ModelUser;

class ControllerLogin {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function exibirTelaUser() {


        
        if ($this->sessao->existe('user')) {
           return $this->response->setContent($this->twig->render("CadastrarUser.twig"));
            
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }
      function exibirTelaAlterarSenha() {


        
        if ($this->sessao->existe('user')) {
           return $this->response->setContent($this->twig->render("AlterarSenha.twig"));
            
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function salvarUsuario() {



        if ($this->sessao->existe('user')) {
            $arrayDeDadosDoForm = $this->request->get("dadosFormulario");
            $usuario = $arrayDeDadosDoForm[0]['value'];
            $senha = $arrayDeDadosDoForm[1]['value'];
            $confsenha = $arrayDeDadosDoForm[2]['value'];
            $tipo = $arrayDeDadosDoForm[3]['value'];

            if (!empty($usuario) && !empty($senha) && !empty($confsenha) && !empty($tipo) && $tipo != 0) {
                if ($senha != $confsenha) {
                    echo '<div class="alert alert-danger" role="alert"> A senha não confere!</div>';
                } else {
                    $senha = sha1($senha);
                    $model = new ModelUser();
                    $user = new User($usuario, $senha, null, $tipo);
                    if ($model->inserirUsuario($user)) {
                        echo '<div class="alert alert-success" role="alert"> Usuário Cadastrado com sucesso!</div>';
                    } else {
                        echo '<div class="alert alert-danger" role="alert"> Há um usuário cadastrado com esse nome!</div>';
                    }
                }
            } else {
                echo '<div class="alert alert-danger" role="alert"> Selecione o tipo de usuário!</div>';
            }
        } else {
        $redirect = new RedirectResponse('/');
        $redirect->send();
        }
    }

    function validarLogin() {

            $arrayDeDadosDoForm = $this->request->get("dadosFormulario");
            $login = $arrayDeDadosDoForm[0]['value'];
            $senha = $arrayDeDadosDoForm[1]['value'];

            if (!empty($login) && !empty($senha)) {
                $senha = sha1($senha);
                $user = new User($login, $senha, null, null);
                $model = new ModelUser();
                $validated = $model->validarLogin($user);
                if ($validated) {
                    $this->sessao->add("user", $validated);
                    echo '<script>window.location.href = "/home"</script>';
                } else {
                    echo '<div class="alert alert-danger" role="alert"> Usuário ou senha são inválidos!</div>';
                }
            } else {
                echo'<div class="alert alert-danger" role="alert">Preencha os campos !! </div>';
            }
        } 
    }

