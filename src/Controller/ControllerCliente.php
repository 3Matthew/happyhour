<?php

namespace HappyHour\Controller;

use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Util\Sessao;
use HappyHour\Entity\Cliente;
use HappyHour\Models\ModelCliente;
use DateTime;

class ControllerCliente {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function exibirTelaCliente() {

        if ($this->sessao->existe('user')) {
            $this->response->setContent($this->twig->render("CadastrarCliente.twig"));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function exibirTelaUpdateCliente() {
        if ($this->sessao->existe('user')) {
            $this->response->setContent($this->twig->render("AtualizarCliente.twig"));
            return;
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function searchCliente($cpf) {

        if ($this->sessao->existe('user')) { 
            $model = new ModelCliente();
            $dados = $model->exibirCliente($cpf);
            return $this->response->setContent($this->twig->render("AtualizarCliente.twig", ['dados' => $dados]));
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function exibirCliente($cpf) {

        if ($this->sessao->existe('user')) {
            $model = new ModelCliente();
            $dados = $model->exibirCliente($cpf);
            return $this->response->setContent($this->twig->render("exibirCliente.twig", ['dados' => $dados]));
        } else {
             $redirect = new RedirectResponse('/cliente/exibir/'.$cpf);
            $redirect->send();
        }
    }

    public function salvarCliente() {


        if ($this->sessao->existe('user')) {
            $nome = $this->request->get("nome");
            $sobrenome = $this->request->get("sobrenome");
            $cpf = $this->request->get("cpf");
            $dataNascimento = new DateTime($this->request->get("data"));
            $dataNascimento = $dataNascimento->format('Y-m-d H:i:s');       
            $email = $this->request->get("email");
            $ddd = $this->request->get("ddd");
            $telefone = $this->request->get("telefone");
            $logradouro = $this->request->get("logradouro");
            $numero = $this->request->get("numero");
            $bairro = $this->request->get("bairro");
            $cep = $this->request->get("cep");
            $complemento = $this->request->get("complemento");
            $cidade = $this->request->get("cidade");
            $estado = $this->request->get("estado");

            if (isset($nome) && !empty($nome) && isset($sobrenome) && !empty($sobrenome) && isset($cpf) && !empty($cpf) && !empty($dataNascimento) && isset($dataNascimento) && !empty($email) && isset($email) && !empty($ddd) && isset($ddd) && !empty($telefone) && isset($telefone) && !empty($logradouro) && isset($logradouro) && !empty($numero) && isset($numero) && !empty($bairro) && isset($bairro) && !empty($cep) && isset($cep) && !empty($cidade) && isset($cidade) && !empty($estado) && isset($estado)) {
                $cliente = new Cliente($nome, $sobrenome, $cpf, $dataNascimento, $logradouro, $numero, $bairro, $cidade, $complemento, $estado, $cep, $ddd, $telefone, $email);
                $modelCliente = new ModelCliente();
                $modelCliente->inserirCliente($cliente);
                $redirect = new RedirectResponse('/cliente/exibir/' . $cpf);
                $redirect->send();
            } else {
                $redirect = new RedirectResponse('/cliente/cadastrar');
                $redirect->send();
            }
        } else {
           $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function updateCliente() {

        if ($this->sessao->existe('user')) {

            $nome = $this->request->get("nome");
            $sobrenome = $this->request->get("sobrenome");
            $cpf = $this->request->get("cpf");
            $dataNascimento = new DateTime($this->request->get("data"));
            $dataNascimento = $dataNascimento->format('Y-m-d H:i:s');  
            $email = $this->request->get("email");
            $ddd = $this->request->get("ddd");
            $telefone = $this->request->get("telefone");
            $logradouro = $this->request->get("logradouro");
            $numero = $this->request->get("numero");
            $bairro = $this->request->get("bairro");
            $cep = $this->request->get("cep");
            $complemento = $this->request->get("complemento");
            $cidade = $this->request->get("cidade");
            $estado = $this->request->get("estado");


            $cliente = new Cliente($nome, $sobrenome, $cpf, $dataNascimento, $logradouro, $numero, $bairro, $cidade, $complemento, $estado, $cep, $ddd, $telefone, $email);
            $modelCliente = new ModelCliente();
            $modelCliente->updateCliente($cliente);
            $redirect = new RedirectResponse('/cliente/exibir/'.$cpf);
            $redirect->send();
        } else {
             $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    public function listarCliente() {

        if ($this->sessao->existe('user')) {
            
            $cpf = $this->request->get("cpf");
            $model = new ModelCliente();
            $dados = $model->exibirCliente($cpf);

            $this->response->setContent($this->twig->render("PesquisarCliente.twig", ['dados' => $dados]));
        } else {
             $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }
    
    public function removerCliente($id) {
         $model = new ModelCliente();
         $model->apagarCliente($Id);
          $redirect = new RedirectResponse('/cliente/pesquisar');
          $redirect->send();
        
        
    }

}
