<?php

namespace HappyHour\Controller;

use HappyHour\Models\ModelProduto;
use Twig\Environment;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use HappyHour\Entity\Produto;
use HappyHour\Util\Sessao;

class ControllerProduto {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    function __construct(Response $response, Environment $twig, Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    function listarProdutos() {
        if ($this->sessao->existe('user')) {


            $model = new ModelProduto();
            $dados = $model->listarProdutos();

            $this->response->setContent($this->twig->render("produtos.twig", ['dados' => $dados]));
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function exibirProduto($id) {


        if ($this->sessao->existe('user')) {


            $model = new ModelProduto();
            $dados = $model->exibirProduto($id);
            return $this->response->setContent($this->twig->render("exibirproduto.twig", ['dados' => $dados]));
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function telaCadastro() {

        if ($this->sessao->existe('user')) {



            return $this->response->setContent($this->twig->render("CadastrarProduto.twig"));
        } else {
             $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function salvarProduto() {

        if ($this->sessao->existe('user')) {

            $nome = $this->request->get("nome");
            $preco = $this->request->get("preco");
            $descricao = $this->request->get("descricao");
            if (isset($descricao) && !empty($descricao) && isset($nome) && !empty($nome) && !empty($preco) && isset($preco)) {
                $produto = new Produto($nome, $preco, $descricao);
                $model = new ModelProduto();
                $id = $model->inserirProduto($produto);
                $redirect = new RedirectResponse('/produto/exibir/' . $id);
                $redirect->send();
            } else {
                $redirect = new RedirectResponse('/produto/cadastrar');
                $redirect->send();
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function atualizarProduto($id) {


        if ($this->sessao->existe('user')) {

            if (isset($id) && !empty($id)) {
                $model = new ModelProduto();
                $produto = $model->exibirProduto($id);
                return $this->response->setContent($this->twig->render("AtualizarProduto.twig", ['produto' => $produto]));
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

    function updateProduto() {


        if ($this->sessao->existe('user')) {

            $id = $this->request->get("id");
            $nome = $this->request->get("nome");
            $preco = $this->request->get("preco");
            $descricao = $this->request->get("descricao");
            if (isset($id) && !empty($id) && isset($descricao) && !empty($descricao) && isset($nome) && !empty($nome) && !empty($preco) && isset($preco)) {
                $produto = new Produto($nome, $preco, $descricao);
                $produto->setId($id);
                $model = new ModelProduto();
                $model->updateProduto($produto);
                $redirect = new RedirectResponse('/produto/exibir/' . $id);
                $redirect->send();
            }
        } else {
            $redirect = new RedirectResponse('/');
            $redirect->send();
        }
    }

}
