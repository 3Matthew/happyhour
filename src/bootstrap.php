<?php
namespace HappyHour;
require __DIR__.'/../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use \Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Twig\Loader\FilesystemLoader;
use Twig\Environment;
use HappyHour\Util\Sessao;

$sessao = new Sessao();
$sessao->start();

include 'rotas.php';


$request = Request::createFromGlobals();

$context = new RequestContext();
$context->fromRequest($request);

$response = Response::create();

$matcher = new UrlMatcher($rotas, $context);

$loader = new FilesystemLoader(__DIR__.'/View/');
$twig = new Environment($loader);

try{
    $atributos = $matcher->match($context->getPathInfo());
    $controller = $atributos['_controller'];
    $method = $atributos['_method'];
    if(isset($atributos['id'])){
        $parametros = $atributos['id'];
    }else{
        $parametros = "";
    }
    $obj = new $controller($response,$twig,$request,$sessao);
    $obj->$method($parametros);
    
    
} catch (ResourceNotFoundException $ex) {
  $response->setContent("Página não encontrada!", $response::HTTP_NOT_FOUND);
}

$response->send();

//print_r($request->query->get('id'));
//echo '</br>';
//print_r($request->query->all());
//echo '</br>';
//print_r($request->getPathInfo());
//echo '</br>';
//print_r($request->getContent());


//print_r($_SERVER);
