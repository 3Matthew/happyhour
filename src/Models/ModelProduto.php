<?php

namespace HappyHour\Models;

use HappyHour\Util\Connection;
use HappyHour\Entity\Produto;
use PDO;

class ModelProduto {

    function __construct() {
        
    }

    public function listarProdutos() {
        try {
            $sql = "select * from produto";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function exibirProduto($id) {

        try {
            $sql = "select * from produto where prod_id = :id";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function inserirProduto(Produto $produto) {

        try {
            $sql = "insert into produto (prod_nome,prod_preco,descricao) values (:nome,:preco,:descricao)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":nome", $produto->getNome());
            $p_sql->bindValue(":preco", $produto->getPreco());
            $p_sql->bindValue(":descricao", $produto->getDescricao());
            if ($p_sql->execute()) {
                return Connection::getInstance()->lastInsertId();
            } else {
                return null;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function updateProduto(Produto $produto) {
        try {

            $sql = "UPDATE produto SET prod_nome = :nome, prod_preco = :preco, descricao = :descricao WHERE prod_id = :id";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":nome", $produto->getNome());
            $p_sql->bindValue(":preco", $produto->getPreco());
            $p_sql->bindValue(":descricao", $produto->getDescricao());
            $p_sql->bindValue(":id", $produto->getId());
            $p_sql->execute();
        } catch (Exception $ex) {
            print 'Erro' . $ex;
        }
    }

}
