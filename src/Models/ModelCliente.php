<?php

namespace HappyHour\Models;

use HappyHour\Util\Connection;
use HappyHour\Entity\Cliente;
use PDO;

class ModelCliente {

    function __construct() {
        
    }

    public function listarClientes() {
        try {
            $sql = "select * from cliente where cli_apagado != 1";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function exibirCliente($cpf) {

        try {
            $sql = "SELECT cli_cpf,cli_nome, DATE_FORMAT(cli_datanasc, '%d/%m/%Y') as cli_datanasc, cli_rua, cli_numero,cli_bairro,cli_complemento,cli_estado,cli_cep,cli_ddd,cli_telefone,cli_email,cli_sobrenome,cli_cidade from cliente where cli_cpf = :cpf and cli_apagado != 1";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cpf", $cpf);
            $p_sql->execute();
            if($p_sql->rowCount() > 0){
            return $p_sql->fetch(PDO::FETCH_ASSOC);    
            }else{
                return FALSE;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function inserirCliente(Cliente $cliente) {

        try {
            $sql = "insert into cliente (cli_cpf,cli_nome,cli_datanasc,cli_rua,cli_numero,cli_bairro,cli_complemento,cli_cidade,cli_estado,cli_cep,cli_ddd,cli_telefone,cli_email,cli_sobrenome) values (:cli_cpf,:cli_nome,:cli_datanasc,:cli_rua,:cli_numero,:cli_bairro,:cli_complemento,:cli_cidade,:cli_estado,:cli_cep,:cli_ddd,:cli_telefone,:cli_email,:cli_sobrenome)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cli_cpf", $cliente->getCpf());
            $p_sql->bindValue(":cli_nome", $cliente->getNome());
            $p_sql->bindValue(":cli_datanasc", $cliente->getDataNascimento() );
            $p_sql->bindValue(":cli_rua", $cliente->getRua());
            $p_sql->bindValue(":cli_numero", $cliente->getNumero());
            $p_sql->bindValue(":cli_bairro", $cliente->getBairro());
            $p_sql->bindValue(":cli_complemento", $cliente->getComplemento());
            $p_sql->bindValue(":cli_cidade", $cliente->getCidade());
            $p_sql->bindValue(":cli_estado", $cliente->getEstado());
            $p_sql->bindValue(":cli_cep", $cliente->getCep());
            $p_sql->bindValue(":cli_ddd", $cliente->getDdd());
            $p_sql->bindValue(":cli_telefone", $cliente->getTelefone());
            $p_sql->bindValue(":cli_email", $cliente->getEmail());
            $p_sql->bindValue(":cli_sobrenome", $cliente->getSobrenome());
            if ($p_sql->execute()) {
                return Connection::getInstance()->lastInsertId();
            } else {
                return null;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function updateCliente(Cliente $cliente) {
        try {
            $sql = "UPDATE cliente SET cli_nome = :cli_nome,cli_datanasc = :cli_datanasc,cli_rua = :cli_rua,cli_numero = :cli_numero,cli_bairro = :cli_bairro,cli_complemento = :cli_complemento,cli_cidade = :cli_cidade,cli_estado = :cli_estado,cli_cep = :cli_cep,cli_ddd = :cli_ddd,cli_telefone = :cli_telefone,cli_email = :cli_email,cli_sobrenome = :cli_sobrenome WHERE cli_cpf = :cli_cpf";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cli_nome", $cliente->getNome());
            $p_sql->bindValue(":cli_datanasc", $cliente->getDataNascimento());
            $p_sql->bindValue(":cli_rua", $cliente->getRua());
            $p_sql->bindValue(":cli_numero", $cliente->getNumero());
            $p_sql->bindValue(":cli_bairro", $cliente->getBairro());
            $p_sql->bindValue(":cli_complemento", $cliente->getComplemento());
            $p_sql->bindValue(":cli_cidade", $cliente->getCidade());
            $p_sql->bindValue(":cli_estado", $cliente->getEstado());
            $p_sql->bindValue(":cli_cep", $cliente->getCep());
            $p_sql->bindValue(":cli_ddd", $cliente->getDdd());
            $p_sql->bindValue(":cli_telefone", $cliente->getTelefone());
            $p_sql->bindValue(":cli_email", $cliente->getEmail());
            $p_sql->bindValue(":cli_sobrenome", $cliente->getSobrenome());
     
            $p_sql->bindValue(":cli_cpf", $cliente->getCpf());
            $p_sql->execute();
            
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }
    public function apagarCliente($Id){
         try {
            $sql = "update cliente set cli_apagado = 1 where cli_cpf = :cpf ";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cpf", $Id);
            if($p_sql->execute()){
                return true;
            }else{
                return false;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }
}
