<?php

namespace HappyHour\Models;

use HappyHour\Util\Connection;
use HappyHour\Entity\Comanda;
use PDO;
use DateTime;


class ModelComanda {

    function __construct() {
        
    }

    public function criarComanda(Comanda $comanda) {
        try {
            $sql = "insert into comanda (cmd_data,Mesa_mes_id) values (CURDATE(),:Mesa_mes_id)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":Mesa_mes_id", $comanda->getMesaId());
            if ($p_sql->execute()) {
                return Connection::getInstance()->lastInsertId();
            } else {
                return null;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function buscarComandasdaMesa($id) {

        try {
            $sql = "select * from comanda where Mesa_mes_id = :id and pago is null";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function addItem($comanda, $produto, $quantidade) {

        try {
            
            $sql = "call addmaisitens(:cmd_id,:prod_id,:quant);";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cmd_id", $comanda);
            $p_sql->bindValue(":prod_id", $produto);
            $p_sql->bindValue(":quant", $quantidade);
            $p_sql->execute();
        } catch (Exception $ex) {
            
        }
    }

    public function detalhesComanda($comanda) {

        try {
            $sql = "select p.prod_id,p.prod_nome,cp.cmd_prod_quant,p.prod_preco,sum(cp.cmd_prod_quant * p.prod_preco) as total from comanda as c join cmd_prod as cp on c.cmd_id = cp.comanda_cmd_id join produto as p on p.prod_id = cp.produto_prod_id  where c.cmd_id = :cmd_id group by p.prod_id ";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cmd_id", $comanda);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function getTotalComanda($comanda) {

        try {
            $sql = "select sum(cp.cmd_prod_quant * p.prod_preco) as total from comanda as c join cmd_prod as cp on c.cmd_id = cp.comanda_cmd_id join produto as p on p.prod_id = cp.produto_prod_id  where c.cmd_id = :cmd_id";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":cmd_id", $comanda);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function buscarComanda($id) {
        try {
            $sql = "SELECT DATE_FORMAT(cmd_data, '%d/%m/%Y') as cmd_data, cmd_id, Mesa_mes_id, cmd_total from comanda where cmd_id = :id;";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function calcularComanda($id) {
        try {
            $sql = "CALL fecha_comanda(:id)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $id);
            $p_sql->execute();
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }
    
    public function pagarComanda($comandaId){
         try {
            $sql = "update comanda set pago = 1 where cmd_id = :id";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":id", $comandaId);
            if($p_sql->execute()){
                return true;
            }else{
                return false;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
        
    }
    
      public function buscarRelatorio( $datain , $datafim) {

        try {
            $sql = "SELECT DATE_FORMAT(cmd_data, '%d/%m/%Y') as cmd_data ,sum(cmd_total) as cmd_total FROM comanda WHERE cmd_data >= :datain AND cmd_data <= :datafim group by cmd_data";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":datain", $datain);
            $p_sql->bindValue(":datafim", $datafim);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

}
