<?php

namespace HappyHour\Models;

use HappyHour\Util\Connection;
use HappyHour\Entity\Mesa;
use PDO;

class ModelMesa {

    function __construct() {
        
    }

    public function listarMesas() {
        try {
            $sql = "select * from mesa";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    public function inserirMesa(Mesa $mesa) {

        try {
            $sql = "insert into mesa (mes_id,mes_quantCadeira,mes_disponivel) values (:mes_id,:mes_quantCadeira,:mes_disponivel)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":mes_id", $mesa->getId());
            $p_sql->bindValue(":mes_quantCadeira", $mesa->getQuantMesa());
            $p_sql->bindValue(":mes_disponivel", $mesa->getDisponivel());
            if ($p_sql->execute()) {
                return Connection::getInstance()->lastInsertId();
            } else {
                return null;
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

    function buscarMesa($id) {

        try {
            $sql = "select * from mesa where mes_id = :mes_id";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":mes_id", $id);
            $p_sql->execute();
            return $p_sql->fetch(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

}
