<?php

namespace HappyHour\Models;

use HappyHour\Util\Connection;
use HappyHour\Entity\User;
use PDO;

class ModelUser {

    function __construct() {
        
    }
    
   

    public function inserirUsuario(User $user) {

        try {
            $sql = "CALL  registrarUsuario (:login,:senha,:tipo)";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":login", $user->getLogin());
            $p_sql->bindValue(":senha", $user->getSenha());
            $p_sql->bindValue(":tipo", $user->getTipo());
            $p_sql->execute();
            $resultado =  $p_sql->fetch(PDO::FETCH_ASSOC);
            if($resultado['resultado'] == 1){
                return false;
            }else{
                return true;
            }
            
        } catch (Exception $ex) {
      
        }
    }

    public function validarLogin(User $user) {

        try {
            $sql = "select * from user where usr_login = :login and usr_senha = :senha";
            $p_sql = Connection::getInstance()->prepare($sql);
            $p_sql->bindValue(":login", $user->getLogin());
            $p_sql->bindValue(":senha", $user->getSenha());
            $p_sql->execute();
            $num = $p_sql->rowCount();
            if($num < 1 ){
                return false;
            }else{
                return $p_sql->fetch(PDO::FETCH_ASSOC);
            }
        } catch (Exception $ex) {
            print 'Erro: ' . $ex;
        }
    }

}
