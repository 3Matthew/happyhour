<?php
namespace HappyHour\rotas;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();
$rotas ->add("inicio", new Route("/",array("_controller" => "HappyHour\Controller\ControllerIndex","_method" => "show")));
$rotas ->add("logado", new Route("/home",array("_controller" => "HappyHour\Controller\ControllerIndex","_method" => "showHome")));
$rotas ->add("login", new Route("/login",array("_controller" => "HappyHour\Controller\ControllerIndex","_method" => "showLogin")));
$rotas ->add("logout", new Route("/logout",array("_controller" => "HappyHour\Controller\ControllerIndex","_method" => "Logout")));
$rotas ->add("cadastrarLogin", new Route("/register",array("_controller" => "HappyHour\Controller\ControllerLogin","_method" => "exibirTelaUser")));
$rotas ->add("validarLogin", new Route("/login/validar",array("_controller" => "HappyHour\Controller\ControllerLogin","_method" => "validarLogin")));
$rotas ->add("salvarLogin", new Route("/register/save",array("_controller" => "HappyHour\Controller\ControllerLogin","_method" => "salvarUsuario")));
$rotas->add("produtos", new Route("/produtos",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "listarProdutos")));
$rotas->add("exibirProdutos", new Route("/produto/exibir/{id}",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "exibirProduto", "id" => "")));
$rotas->add("clientes", new Route("/cliente/pesquisar",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "listarCliente")));
$rotas->add("cadastrarProduto", new Route("/produto/cadastrar",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "telaCadastro")));
$rotas->add("salvarProduto", new Route("/produto/insert",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "salvarProduto")));
$rotas->add("atualizarProduto", new Route("/produto/atualizar/{id} ",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "atualizarProduto", "id" => "")));
$rotas->add("updateProduto", new Route("/produto/update",array("_controller" => "HappyHour\Controller\ControllerProduto","_method" => "updateProduto")));
$rotas->add("cadastrarCliente", new Route("/cliente/cadastrar",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "exibirTelaCliente")));
$rotas->add("removeCliente", new Route("/cliente/remove/{id}",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "removerCliente", "id" => "")));
$rotas->add("salvarCliente", new Route("/cliente/insert",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "salvarCliente")));
$rotas->add("atualizarCliente", new Route("/cliente/atualizar",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "exibirTelaUpdateCliente")));
$rotas->add("searchCliente", new Route("/cliente/search/{id}",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "searchCliente", "id"=> "" )));
$rotas->add("atualizarUpdate", new Route("/cliente/update",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "updateCliente")));
$rotas->add("exibirCliente", new Route("/cliente/exibir/{id}",array("_controller" => "HappyHour\Controller\ControllerCliente","_method" => "exibirCliente", "id" => "")));
$rotas->add("cadastrarMesa", new Route("/mesa/cadastrar",array("_controller" => "HappyHour\Controller\ControllerMesa","_method" => "exibirTelaMesa")));
$rotas->add("salvarMesa", new Route("/mesa/insert",array("_controller" => "HappyHour\Controller\ControllerMesa","_method" => "salvarMesa")));
$rotas->add("exibirMesa", new Route("/mesa/exibir/{id}",array("_controller" => "HappyHour\Controller\ControllerMesa","_method" => "exibirMesa", "id" => "")));
$rotas->add("criarComanda", new Route("/mesa/novacomanda/{id}",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "criarComanda", "id" => "")));
$rotas->add("addItensComanda", new Route("/comanda/{id}",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "exibirProdutos", "id" => "")));
$rotas->add("escolherItens", new Route("/comandas/insert",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "addItensComanda")));
$rotas->add("exibirComandas", new Route("/mesa/comandas/{id}",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "exibirComandas", "id" => "")));
$rotas->add("MostrarMesas", new Route("/mesas",array("_controller" => "HappyHour\Controller\ControllerMesa","_method" => "mostrarTodas")));
$rotas->add("cadastrarUsuario", new Route("/usuario/cadastrar",array("_controller" => "HappyHour\Controller\ControllerUser","_method" => "exibirTelaUser")));
$rotas->add("salvarUsuario", new Route("/usuario/insert",array("_controller" => "HappyHour\Controller\ControllerUser","_method" => "salvarMesa")));
$rotas->add("fecharFatura", new Route("/checkout/{id}",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "fecharComanda", "id" => "")));
$rotas->add("detalhesComanda", new Route("/comanda/detalhes/{id}",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "verDetalhesComanda", "id" => "")));
$rotas->add("calculaValorAvista", new Route("/checkout/calcular",array("_controller" => "HappyHour\Controller\ControllerFatura","_method" => "pagarAvista")));
$rotas->add("buscarCliente", new Route("/checkout/buscar",array("_controller" => "HappyHour\Controller\ControllerFatura","_method" => "buscarCpf")));
$rotas->add("GerarBoleto", new Route("/boleto",array("_controller" => "HappyHour\Controller\ControllerFatura","_method" => "gerarBoleto")));
$rotas->add("relatorio", new Route("/relatorio",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "gerarRelatorio")));
$rotas->add("exibirRelatorio", new Route("/relatorio/show",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "showRelatorio")));
$rotas->add("alterarSenha", new Route("/login/senha/alterar",array("_controller" => "HappyHour\Controller\ControllerLogin","_method" => "exibirTelaAlterarSenha")));
$rotas->add("alterarSenha", new Route("/calcular",array("_controller" => "HappyHour\Controller\ControllerComanda","_method" => "CalcularAvista")));

return $rotas;

