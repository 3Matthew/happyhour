<?php

namespace HappyHour\Entity;
use DateTime;


class Comanda {

    private $comanId;
    private $comanData;
    private $comanTotal;
    private $mesaId;

    function __construct($comanId, DateTime $comanData, $comanTotal, $mesaId) {
        $this->comanId = $comanId;
        $this->comanData = $comanData;
        $this->comanTotal = $comanTotal;
        $this->mesaId = $mesaId;
    }
    
    function getComanId() {
        return $this->comanId;
    }

    function getComanData() {
        return $this->comanData;
    }

    function getComanTotal() {
        return $this->comanTotal;
    }

    function getMesaId() {
        return $this->mesaId;
    }

    function setComanId($comanId) {
        $this->comanId = $comanId;
    }

    function setComanData($comanData) {
        $this->comanData = $comanData;
    }

    function setComanTotal($comanTotal) {
        $this->comanTotal = $comanTotal;
    }

    function setMesaId($mesaId) {
        $this->mesaId = $mesaId;
    }

}
