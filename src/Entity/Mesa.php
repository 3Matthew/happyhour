<?php

namespace HappyHour\Entity;

class Mesa {

    private $id;
    private $quantMesa;
    private $disponivel;

    function __construct($id, $quantMesa, $disponivel) {
        $this->id = $id;
        $this->quantMesa = $quantMesa;
        $this->disponivel = $disponivel;
    }

    function getId() {
        return $this->id;
    }

    function getQuantMesa() {
        return $this->quantMesa;
    }

    function getDisponivel() {
        return $this->disponivel;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setQuantMesa($quantMesa) {
        $this->quantMesa = $quantMesa;
    }

    function setDisponivel($disponivel) {
        $this->disponivel = $disponivel;
    }

}
