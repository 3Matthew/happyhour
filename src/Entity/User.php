<?php

namespace HappyHour\Entity;

class User {

    private $login;
    private $senha;
    private $token;
    private $tipo;

    function __construct($login, $senha, $token, $tipo) {
        $this->login = $login;
        $this->senha = $senha;
        $this->token = $token;
        $this->tipo = $tipo;
    }

    function getLogin() {
        return $this->login;
    }

    function getSenha() {
        return $this->senha;
    }

    function getToken() {
        return $this->token;
    }

    function getTipo() {
        return $this->tipo;
    }

    function setLogin($login) {
        $this->login = $login;
    }

    function setSenha($senha) {
        $this->senha = $senha;
    }

    function setToken($token) {
        $this->token = $token;
    }

    function setTipo($tipo) {
        $this->tipo = $tipo;
    }
}